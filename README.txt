
README
======

Requirements: A web browser with very good web standards support.

At the moment to get full benefit from these stylesheets you will need to use a
web browser with  support for the css genereated content
(the 'content' property) - currently (early 2006) that means a recent
version of Firefox, Safari, Opera or Konqueror 
(also any browser which uses the same rendering engine as those). 
Note that forthcoming IE7 *still* won't support generated content.


themedev diagnostic CSS stylesheets
===================================

Several 'diagnostic CSS' stylesheets are included which highlight page structure
and styling. These can be turned on or off in the themedev module settings.
Currently there are seven stylesheets, a short description of each follows:

skeletal.css
This flags most xhtml tags, especially those with id and class
attributes, and outlines page structure.

form.css
Highlights and outlines form elements - note that support for generated content
on form elements is not great even in Firefox, so much of this stylesheet
doesn't do anything.

lists.css
This flags list beginning and end tags with id and class attributes if
present, and outlines list structure. It can make drupal menu's difficult to
read, but does allow you to see their layout and style tags.

table.css
This outlines table structure and flags id and class attributes on
table elements.

deprecated-xhtml1strict.css
This will only be useful if you have a theme with an
xhtml 1.0 strict doctype. This will highlight html tags which are deprecated in
the xhtml 1.0 strict doctype in red. Also highlights proprietary tags bright
magenta.

abused.css
This highlights often abused tags like blockquote in red-orange
(blockquote is often used to indent text, it is intended for the quoting of
large passages of text). It will highlight all instances of the tags, not actual
cases of abuse, so these serve as a warning, rather than highlighting an error.

usability.css
This highlights possible usability problems. Such as images with
no alt tags, links that open in new windows and title attributes - used for
'tooltips' in many web browsers. Again, this stylesheet doesn't highlight errors
as such, but can allow you to see where errors have been made, such as missing a
title attribute on only one of a list of links.

By default the styles are not visible to the admin with uid=1 (the first account
created when a drupal site is installed) to ensure that the stylesheets can
always be disabled by this admin. For best results create a 'theme developer'
role, enable the 'access themedev css' permission for this role and apply the
role to a user account. Use this theme developer account to view the site with
themedev css enabled, and another account to see what the site looks like
without them. In other words use the theme developer account to identify content
you want to style differently and the other account to view the effects of your
changes.


themedev block
==============

As of 4.7 themedev module also outputs as a block an abbrieviated form of the
settings page. The block is fixed in position and thus should have little effect
on the layout *unless* it's the only block output in a specific region - so if
you have no other blocks enabled create a specific phptemplate region for the
block, after all the other content. By default the block appears at the lower
left of the browser window, double clicking on the block will move it to the
upper right of the window (unfortunately if you refresh the page or go to a
different one it will move back).
  NOTE that we now have per theme block settings - if the block doesn't appear on your new theme make sure the block is enabled for that theme!

If you still feel you need to view the themedev css with the first admin user
you can enable this on the settings page by turning 'Enable Viewing For Admin'
to 'On'.

There is a demonstration site at:
<http://themedev.perlucida.com/>
If you'd like to see the module in action, you'll need to log in with your
drupal ID.

Adrian Simmons
adrinux@perlucida.com
