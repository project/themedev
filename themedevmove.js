// used to move the block around the screen

if (isJsEnabled()) {
  addLoadEvent(themedevMoveAutoAttach);
}

function themedevMoveAutoAttach() {
	var block = document.getElementById('block-themedev-0');
			block.ondblclick = function() {
				block.style.left = (block.style.left != 'auto' ? 'auto' : '0px' );
				block.style.right = (block.style.right != '0px' ? '0px' : 'auto' );
				block.style.bottom = (block.style.bottom != 'auto' ? 'auto' : '0px' );
				block.style.top = (block.style.top != '15px' ? '15px' : 'auto' );
			}
}
